
#ifndef _COG_RADAR_
#define _COG_RADAR_

#include <string>
#include <functional>
#include <vector>
#include <iostream>
#include "cogDataProvider.h"

// Информация о радаре. 
// Например: HardVersion: "3.1" - версия плат радара
//           SoftVersion: "2.1161" - версия ПО
//           SerialNumb:  "418165672" - десятичный идентификатор радара
struct TCogRadarInfo {
    std::string HardVersion;
    std::string SoftVersion;
    std::string SerialNumb;
};

// Формат представления версии софта/железа
struct TCogRadarVersionFrmt {
    uint16_t minor;
    uint16_t major;
    uint16_t patch;
};

// Резерв. Параметры движения платформы, на которой закреплен радар
// в текущей версии радара, параметры не учитываются.
struct TCogRadarSetPlatform {
    double velocity;
    double yaw_rate;
    int    direction;
};

// Параметры отметки.
// В текущей версии ПО существует 2 режима выдачи:
//  - режим выдачи треков;
//  - режим выдачи детекций;
typedef struct {           //     треки       |     детекции    | размерность
    int64_t ObjID;         // id трека        | индекс 0..255   | -
    double Range;          // дальность       | дальность       | М
    double Azimuth;        // угол            | угол            | градусы
    int64_t LiveTime;      // время жизни     | 100             | мс
    double Rcs;            // энергия отметки | энергия отметки | у.е.
    double Xcoord;         // коорд. X        | коорд. X        | М
    double Xrate;          // скорость по X   | 0               | М/С
    double Xacceleration;  // ускорение по X  | 0               | М/C^2
    double Ycoord;         // коорд. Y        | коорд. Y        | М
    double Yrate;          // скорость по Y   | радиал.скорость | М/С
    double Yacceleration;  // ускорение по Y  | 0               | М/C^2
} TCogRadarTarget;

/*
  Статус:
----------------------------------------------------------------
  ONLINE    | Соединение с радаром установлено и радар выдает пакеты
  OFFLINE   | Соединения с радаром нет или нет пакетов от радара
 INCORRECT  | Ошибка открытия сокета
----------------------------------------------------------------
*/

enum class TCogRadarStatus { ONLINE, OFFLINE, INCORRECT };

std::ostream& operator<<(std::ostream& out, const TCogRadarStatus value) {
    if (value == TCogRadarStatus::ONLINE)
        return out << "ONLINE";
    else if (value == TCogRadarStatus::OFFLINE)
        return out << "OFFLINE";
    else if (value == TCogRadarStatus::INCORRECT)
        return out << "ERROR";
    return out << "UNDEF";
}

enum class TCogRadarSendMode { 
    UNDEF,       // режим не определен
    STREAM,      // режим потоковой выдачи отметок (детекций или треков) 
    REQUEST,     // режим выдачи отметок (детекций или треков) по запросу 
    };

enum class TCogRadarTrackingMode {
    UNDEF,       // режим не определен
    ON,          // режим выдачи отметок-треков (используется трекинг)
    OFF          // режим выдачи отметок-детекций (трекинг не используется)
};


// Обработчики событий приема отметок и статуса.
// Аргумент: arg - произвольный пользовательский параметр. 
// Его значение будет передаваться при вызове обработчика события
typedef std::function<void (
    uint64_t timeStamp,      // метка времени кадра в мкс, начиная с момента вкл. радара;
    uint64_t grabNumber,     // последовательный номер кадра, начиная с момента вкл. радара;
    std::vector<TCogRadarTarget>& targets, // список отметок
    int userArg              // аргумент пользователя 
    ) > TCogFrameEvent;
typedef std::function<void(const TCogRadarStatus status, int userArg) > TCogRadarStatusEvent;


class TCogRadar {
public:
    virtual ~TCogRadar() {};
    virtual TCogRadarStatus open(const std::string&  ip, uint16_t port) = 0;
    virtual bool setOption(const std::string& key, const std::string& value) = 0;
    virtual bool setOption(const std::string& key, int value) = 0;
    virtual bool close() = 0;
    virtual bool start(TCogRadarSendMode sendMode, TCogRadarTrackingMode trackingMode, TCogFrameEvent frameDoneEvent, int userArg) = 0;
    virtual bool stop() = 0;
    virtual TCogRadarInfo & getVersion() = 0;
    virtual TCogRadarVersionFrmt& getSoftVersion() = 0;
    virtual TCogRadarVersionFrmt& getHardVersion() = 0;
    virtual bool setPlatformInfo(const TCogRadarSetPlatform& info) = 0;
    virtual bool set(const std::string& cmdString) = 0;
    virtual std::string get(const std::string& cmdString) = 0;
    virtual bool subscribeRadarStatus(TCogRadarStatusEvent radarStatusEvent, int userArg) = 0;
    virtual bool unSubscribeRadarStatus() = 0;
    virtual TCogRadarStatus getRadarStatus() = 0;
    // Возможность задать пользовательский канал приема данных (пока не актуально)
    // virtual bool setDataProvider(TCogDataProvider* dataProvider) = 0;
};


#endif