#ifndef _COG_RADAR_DEMO_
#define _COG_RADAR_DEMO_
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>
#include <chrono>
#include <functional>
#include <cmath>

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#define DEMO_TARGET_NUM 40
#define DEMO_TARGET_FPS 10


#define DEMO_DELAY_MS   (1000 / DEMO_TARGET_FPS)

class TCogRadarDemo : public TCogRadar {
private:
    TCogRadarInfo radarInfo = {"0.1", "0.9", "12345678"};
    TCogRadarVersionFrmt softInfo = {0, 9};
    TCogRadarVersionFrmt hardInfo = { 1, 2 };
    bool radarUp = false;
    bool radarStart = false;
    std::vector<TCogRadarTarget> radarTargets;
    std::unique_ptr<std::thread> radarThread;
    bool doExit = false;
    uint64_t TimeStamp = 0;
    uint64_t GrabNumber = 0;
    TCogRadarStatus radarStatus = TCogRadarStatus::OFFLINE;

    // ������� ������������ ��� ������ ������� � �������
    TCogFrameEvent userFrameDoneEvent = NULL;
    int userFrameDoneArg = 0;
    TCogRadarStatusEvent userRadarStatusEvent = NULL;
    int userRadarStatusArg = 0;


    // ��������� ������� ������.
    // ��� ��������� ������� ������ ������ ����������� ������������
    void setRadarStatus(const TCogRadarStatus& status) {
        bool statusChanged = (radarStatus != status);
        radarStatus = status;
        // ������ ���������. ������ ������������ ����� ��������
        if (userRadarStatusEvent && statusChanged) {
            userRadarStatusEvent(status, userRadarStatusArg);
        }
    }

public:

    TCogRadarDemo() {
    };

    ~TCogRadarDemo() {
    };

    TCogRadarInfo & getVersion() {
        return radarInfo;
    }

    TCogRadarVersionFrmt& getSoftVersion() {
        return softInfo;
    }

    TCogRadarVersionFrmt& getHardVersion() {
        return hardInfo;
    }

    bool setPlatformInfo(const TCogRadarSetPlatform& info) {
        return true;
    }

    bool start(TCogRadarSendMode sendMode, TCogRadarTrackingMode trackingMode, TCogFrameEvent frameDoneEvent, int userArg) {
        userArg = userArg;
        userFrameDoneEvent = frameDoneEvent;
        radarStart = true;
        return true;
    }

    bool stop() {
        radarStart = false;
        return true;
    }

    bool set(const std::string& cmdString) {
        return true;
    }

    std::string get(const std::string& cmdString) {
        return "";
    }

    bool setOption(const std::string& key, const std::string& value) {
        return true;
    }

    bool setOption(const std::string& key, int value) {
        return true;
    }


    TCogRadarStatus open(const std::string& ip, uint16_t port) {
        return open();
    }

    TCogRadarStatus open() {
        if (radarThread) {
            return TCogRadarStatus::INCORRECT;
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            radarThread = std::make_unique<std::thread>([this] { this->readingLoop(); });
            setRadarStatus(TCogRadarStatus::ONLINE);
            radarUp = true;
            return TCogRadarStatus::ONLINE;
        }
    }

    bool close() {
        if (radarThread) {
            radarUp = false;
            doExit = true;
            radarThread->join();
            radarThread.reset(nullptr);
            setRadarStatus(TCogRadarStatus::OFFLINE);
            return true;
        }
        return false;
    }

    void rxEvent(void * buffer, unsigned int size) {
    }

    void rxEvent(std::istream& in) {
    }

    TCogRadarStatus getRadarStatus() {
        return TCogRadarStatus::ONLINE;
    }

    bool subscribeRadarStatus(TCogRadarStatusEvent radarStatusEvent, int userArg) {
        userRadarStatusEvent = radarStatusEvent;
        userRadarStatusArg = userArg;
        return true;
    }
    bool unSubscribeRadarStatus() {
        userRadarStatusEvent = NULL;
        userRadarStatusArg = 0;
        return true;
    }
private:

    bool readingLoop() {
        doExit = false;
        while (!doExit) {
            std::this_thread::sleep_for(std::chrono::milliseconds(DEMO_DELAY_MS));
            TimeStamp += DEMO_DELAY_MS * 1000;
            GrabNumber++;
            if (radarStart && radarUp) {
                getTarget(DEMO_TARGET_NUM);
                if (userFrameDoneEvent) {
                    userFrameDoneEvent(TimeStamp, GrabNumber, radarTargets, userFrameDoneArg);
                }
            }
        }
        return true;
    }


    void targetGeneratorCircle(int indexOfTarget, int numTarget, int id, TCogRadarTarget& target) {
        static int time = 0;
        static double dAngle = 0.0;
        double R = 30.0;
        double L = 40.0;
        if (indexOfTarget == 0) {
            dAngle += 0.1;
            time++;
        }
        double a = dAngle + ((float)indexOfTarget / (float)numTarget) * 3.1415926535 * 2.0;
        double x = R * cos(a);
        double y = R * sin(a) + L;
        target.ObjID = id;
        target.Xcoord = x;
        target.Ycoord = y;
        target.Rcs = -100.000 + 0.001 * (float)indexOfTarget;
        target.Xacceleration = 0;
        target.Yacceleration = 0;
        target.Xrate = 0;
        target.Yrate = 0;
        target.LiveTime = time;
        target.Range = sqrt(x * x + y * y);
        target.Azimuth = atan(x / y) * 180.0 / 3.1415926535;
    }

    void getTarget(unsigned int targetNum) {
        static int64_t LiveTime = 0;
        LiveTime += DEMO_DELAY_MS * 1000;
        radarTargets.resize(targetNum);
        int tCount = 0;
        for (auto& Target : radarTargets) {
            targetGeneratorCircle(tCount, targetNum, tCount + 1, Target);
            Target.LiveTime = LiveTime;
            tCount++;
        }
    }
};


#endif
