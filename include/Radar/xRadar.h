#ifndef _COG_RADAR_X_
#define _COG_RADAR_X_
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#include "../Radar/cprr/radarDriver_cprr.h"
#include "../Radar/demo/radarDriver_demo.h"

#define COG_RADAR_DEMO  0  // Emulator(demo)
#define COG_RADAR_CPRR  1  // CPRR 24GGz


class TCogRadarX : public TCogRadar {
private:
    std::unique_ptr<TCogRadar> radar;
    TCogRadarInfo radarInfo = {"undef", "undef", "undef"};
    TCogRadarVersionFrmt softInfo = {0, 0};
    TCogRadarVersionFrmt hardInfo = {0, 0};
public:


    TCogRadarX(int radarCode) {
        if (radarCode == COG_RADAR_CPRR) {
            radar = std::make_unique<TCogRadarCPRR>();
        } else if (radarCode == COG_RADAR_DEMO) {
            radar = std::make_unique<TCogRadarDemo>();
        }
    };

    // Returns version information (hardware version and software version of the radar) as text (std::string).
    TCogRadarInfo & getVersion() {
        return (radar) ? radar->getVersion() : radarInfo;
    }

    // Returns software version of the radar as major.minor (uint16_t)
    TCogRadarVersionFrmt& getSoftVersion() {
        return (radar) ? radar->getSoftVersion() : softInfo;
    }

    TCogRadarVersionFrmt& getHardVersion() {
        return (radar) ? radar->getHardVersion() : hardInfo;
    }

    bool setPlatformInfo(const TCogRadarSetPlatform& info) {
        return (radar) ? radar->setPlatformInfo(info) : false;
    }

    bool start(TCogRadarSendMode sendMode, TCogRadarTrackingMode trackingMode, TCogFrameEvent frameDoneEvent, int userArg) {
        return (radar) ? radar->start(sendMode, trackingMode, frameDoneEvent, userArg) : false;
    }

    bool stop() {
        return (radar) ? radar->stop() : false;
    }

    bool set(const std::string& cmdString) {
        return (radar) ? radar->set(cmdString) : false;
    }

    std::string get(const std::string& cmdString) {
        return (radar) ? radar->get(cmdString) : "";
    }

    bool setOption(const std::string& key, const std::string& value) {
        return (radar) ? radar->setOption(key, value) : false;
    }

    bool setOption(const std::string& key, int value) {
        return (radar) ? radar->setOption(key, value) : false;
    }

    // Возвращаемое значение нужно обработать
    [[nodiscard]] TCogRadarStatus open(const std::string& ip, uint16_t port) {
        return (radar) ? radar->open(ip, port) : TCogRadarStatus::INCORRECT;
    }

    bool close() {
        return (radar) ? radar->close() : false;
    }

    TCogRadarStatus getRadarStatus() {
        return (radar) ? radar->getRadarStatus() : TCogRadarStatus::INCORRECT;
    }

    bool subscribeRadarStatus(TCogRadarStatusEvent radarStatusEvent, int userArg) {
        return (radar) ? radar->subscribeRadarStatus(radarStatusEvent, userArg) : false;
    }

    bool unSubscribeRadarStatus() {
        return (radar) ? radar->unSubscribeRadarStatus() : false;
    }

};


#endif
