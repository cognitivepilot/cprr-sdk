#ifndef _COG_PROTOCOL_CPRR
#define _COG_PROTOCOL_CPRR

#include <string>
#include <functional>
#include <iostream>
#include <ostream>
#include <sstream>

#include "protocolcmdList.h"

class TCogProtocolFrameCPRR : public TCogProtocolFrame {
private:
    TRadarPacket pkt;
    bool stxFlag = false;
    unsigned int rxSize = 0;
public:
    int unpack(std::istream& in, std::function<void (void *, unsigned int) > frameEvent) {
        int pktCount = 0;
        unsigned char c;

        in.seekg(0, in.end);
        int length = (int)in.tellg();
        in.seekg(0, in.beg);

        while (!in.eof() && (length > 0)) {
            char * ptr = (char *) &pkt;
            if (!stxFlag) {
                // пока заголовок не подтвержден, читаем из потока по 1 байту
                in.read((char*)&c, 1);

                length--;
                if (rxSize < sizeof(TRadarHeaderMessage)) {
                    stxFlag = false;
                    ptr[rxSize++] = c;
                }
                if (rxSize == sizeof(TRadarHeaderMessage)) {
                    if (pkt.hdr.preamble != RADAR_PKT_PREAMBLE) {
                        rxSize--;
                        for (unsigned int i = 0; i < rxSize; i++) {
                            ptr[i] = ptr[i + 1];
                        }
                    } else {
                        // STX OK
                        stxFlag = true;
                    }
                }
            }
            if (stxFlag) {
                if (pkt.hdr.length > 2000) {
                    stxFlag = false;
                    rxSize = 0;
                } else {
                    unsigned int messageTailSize = sizeof (TRadarHeaderMessage) + pkt.hdr.length - rxSize;
                    unsigned int availableSize = (messageTailSize <= (unsigned int)length) ? messageTailSize : (unsigned int)length;
                    if (availableSize)
                        in.read(&ptr[rxSize], availableSize);
                    length -= availableSize;
                    rxSize += availableSize;
                    messageTailSize -= availableSize;

                    if (messageTailSize == 0) {
                        if (frameEvent) {
                            frameEvent((void*) ptr, rxSize);
                        }
                        stxFlag = false;
                        rxSize = 0;
                        pktCount++;
                    }
                }
            }
        }
        return pktCount;
    }
};

class TCogProtocolCPRR { 
public:

    bool init(std::string initString) {
        return false;
    }
    
    // Формат сообщения запроса версии радара
    TCogProtocolCPRR & reqGetVersion() {
        pkt.hdr.pktType = RADAR_PKT_REQUEST_ID;
        pkt.message.request = RADAR_REQ_GETVER;
        messageSize = sizeof (pkt.message.request);
        return *this;
    }

    // Сообщение установки режима работы радара
    // enable : true - радар переходит в режим излучения
    //          false - режим излучения выключен
    // streamMode : true - режим выдачи отметок по готовности (без запроса)
    //              false - режим выдачи отметок только по запросу
    // trackingMode : true - трекинг включен;
    //                false - трекинг отключен.
    TCogProtocolCPRR & reqSetMode(bool enable, bool streamMode, bool trackingMode) {
        pkt.hdr.pktType = RADAR_PKT_MODE_ID;
        pkt.message.mode.power = (enable) ? 1 : 0;
        if (enable) {
            if (!trackingMode)
                pkt.message.mode.power = 1000; // код режима сырых детекций
        }
        pkt.message.mode.streaming = (streamMode) ? 1 : 0;
        messageSize = sizeof (pkt.message.mode);
        return *this;
    }

    // Сообщение передачи в радар данных о движении платформы
    // (фактически радаром CPRR-24 не обрабатывается)
    TCogProtocolCPRR & reqSetPlatform(double velocity, double yaw_rate, bool forward) {
        pkt.hdr.pktType = RADAR_PKT_PLATFORM_ID;
        pkt.message.platform.Velocity = velocity;
        pkt.message.platform.Yaw_rate = yaw_rate;
        pkt.message.platform.Forward = (forward) ? 1 : 0;
        messageSize = sizeof (pkt.message.platform);
        return *this;
    }


    std::stringstream& pack(std::stringstream&& out) {
        return pack(out);
    }

    std::stringstream& pack(std::stringstream& out) {
        pkt.hdr.preamble = RADAR_PKT_PREAMBLE;
        pkt.hdr.length = messageSize;
        int pktSize = messageSize + sizeof (pkt.hdr);
        out.write((const char *) &pkt, pktSize);
        return out;
    }

    int unpack(std::istream& in, std::function<void (void *, unsigned int) > frameEvent) {
        return cprrFrame.unpack(in, frameEvent);
    }

private:
    int messageSize = 0;
    TRadarPacket pkt;
    TCogProtocolFrameCPRR cprrFrame;
};


#endif
