
#ifndef RADAR_V2_H
#define RADAR_V2_H
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

   
// preamble
#define RADAR_PKT_PREAMBLE              0x0000ABCD
   
// message type identifiers 
#define RADAR_PKT_REQUEST_ID            1
#define RADAR_PKT_MODE_ID               2
#define RADAR_PKT_PLATFORM_ID           3
#define RADAR_PKT_DATA_ID               4
#define RADAR_PKT_INFO_ID               5
#define RADAR_PKT_ERROR_ID              6
#define RADAR_PKT_MIN_ID                1
#define RADAR_PKT_MAX_ID                6

//subCode for REQUEST_ID
#define RADAR_REQ_GETVER                1

#define RADAR_PKT_TARGET_MAX_NUM        16


#pragma pack(push, 1)

#define GCC_PACK_ATTR   //__attribute__((__packed__)) 

typedef struct GCC_PACK_ATTR {
    // preamble
    uint32_t preamble;
    // Data field length
    uint32_t length;
    // message type identifier 
    uint32_t pktType;
} TRadarHeaderMessage;

typedef struct GCC_PACK_ATTR {
    uint32_t power;
    uint32_t streaming;
} TRadarPackMode;


typedef struct GCC_PACK_ATTR {
    double   Velocity;
    double   Yaw_rate;
    uint64_t Forward;
} TRadarPackPlatform;

typedef struct GCC_PACK_ATTR {
    uint32_t Error;
} TRadarPackError;

typedef struct GCC_PACK_ATTR {
    int64_t ObjID;
    double Range;
    double Azimuth;
    int64_t LiveTime;
    double Rcs;
    double Xcoord;
    double Xrate;
    double Xacceleration;
    double Ycoord;
    double Yrate;
    double Yacceleration;
} TRadarTarget;

typedef struct GCC_PACK_ATTR {
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
} TRadarTargetParam;

typedef struct GCC_PACK_ATTR {
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
    TRadarTarget Targets [RADAR_PKT_TARGET_MAX_NUM];
} TRadarPackData;

typedef struct GCC_PACK_ATTR {
    uint16_t major;
    uint16_t minor;
    uint16_t patch;
}TVersionFormat;

typedef struct GCC_PACK_ATTR {
    TVersionFormat HardVersion;
    TVersionFormat SoftVersion;
    uint32_t SerialNumb;
} TRadarPackInfo;


typedef union GCC_PACK_ATTR {
    uint32_t                 request; // Buffer for request code 
    TRadarPackMode           mode;    // Buffer for MODE_ID (power and radar mode) 
    TRadarPackPlatform       platform;// Buffer for PLATFORM_ID (reserved) 
    TRadarPackInfo           verInfo; // Buffer for radar version information (SN: xxxxxxxx, SW : xx.xx, HW: xx.xx)
    TRadarPackData           data;    // Buffer for the DATA_ID package. (receiving a list of targets) 
    TRadarPackError          error;   // Buffer for radar error code
    char                     buffer[1];//Buffer as byte buffer 
} TRadarCmdList;

typedef struct GCC_PACK_ATTR {
    TRadarHeaderMessage   hdr;
    TRadarCmdList         message;
} TRadarPacket;

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif /* RADAR_V2_H */

