#ifndef _COG_RADAR_CPRR_
#define _COG_RADAR_CPRR_
#include <thread>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip> 
#include <stdexcept>
#include <chrono>

#include <condition_variable>
#include <memory>
#include <mutex>

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#include <DataProvider/udpDataProvider.h>
#include <DataProvider/tcpDataProvider.h>
#include "../cprr/protocol.h"

// кол-во попыток запросить версию радара. Запрос версии радара используется как тест присутствия радара
#define CPRR_RADAR_GETVER_TRY_NUM		3
// Пауза между попытками запроса версии радара
#define CPRR_RADAR_GETVER_DELAY_MS		1000
// Интервал мониторинга состояния радара
#define CPRR_RADAR_ONLINE_MONITORING_INTERVAL_MS		3000

//
// Класс доступа к CPRR24
// 

class TCogRadarCPRR : public TCogRadar {
private:
	bool radarActivityFlag = false;
	TCogRadarInfo radarInfo = {"undef", "undef", "undef"};
	TCogRadarVersionFrmt softInfo = {0, 0, 0};
	TCogRadarVersionFrmt hardInfo = {0, 0, 0};

	// Драйвер канала связи с радаром и его мьютекс
	std::unique_ptr<TCogDataProvider> dataProvider;
	std::mutex dataProviderMutex;

	TCogRadarStatus radarStatus = TCogRadarStatus::INCORRECT;

	volatile bool radarInfoUpdate = false;
	volatile bool networkError = false;
	volatile bool doExit = false;

	TCogProtocolCPRR cprrProtocol;

	// колбэки пользователя для приема отметок и статуса
	TCogFrameEvent userFrameDoneEvent = NULL;
	int userFrameDoneArg = 0;
	TCogRadarStatusEvent userRadarStatusEvent = NULL;
	int userRadarStatusArg = 0;
	std::vector<TCogRadarTarget> radarTargets;
	
	// Код ошибки радара, если он выдавал пакет RADAR_PKT_ERROR_ID
	uint32_t lastErrorCode = 0;

	// Нужно для периодического запроса статуса
	std::unique_ptr<std::thread> readingStatusThread;
	volatile bool isReadingStatusRun = false;
	std::condition_variable readingStatusIntervalSignal;
	
	// опции полученные перед открытием
	std::map<std::string, std::string> options;

	// Установка статуса радара.
	// При изменении статуса радара выдача уведомления пользователю
	void setRadarStatus(const TCogRadarStatus& status) {
		bool statusChanged = (radarStatus != status);
		radarStatus = status;
		// Статус изменился. выдаем пользователю новое значение
		if (userRadarStatusEvent && statusChanged) {
				userRadarStatusEvent(status, userRadarStatusArg);
		}
	}

	// поток опроса состояния радара. Актуально для режима MANUAL (отметки по запросу)
	// и для случая переподключения радара

	void readingStatusLoop() {
		std::mutex mx;
		std::unique_lock<std::mutex> pauseLock(mx);

		bool reqVersion = false;

		isReadingStatusRun = true;

		while (!doExit) {
			readingStatusIntervalSignal.wait_for(pauseLock, std::chrono::milliseconds(CPRR_RADAR_ONLINE_MONITORING_INTERVAL_MS));

			if (!doExit) {
				if (radarActivityFlag) {
					// от радара идут данные, дополнительных запросов выдавать не нужно
					reqVersion = false;
				} else {
					// За интервал ожидания не было пакетов от радара
					if (reqVersion) {
						// Был запрос. Ответа нет.
						setRadarStatus(TCogRadarStatus::OFFLINE);
						reqVersion = false;
					} else {
						// Формируем запрос версии
						std::lock_guard<std::mutex> lock(dataProviderMutex);
						if (dataProvider) {
							if (dataProvider->write(cprrProtocol.reqGetVersion().pack(std::stringstream{})) < 0) {
								// Проблемы с выдачей в сеть
								networkError = true;
								setRadarStatus(TCogRadarStatus::OFFLINE);
							} else {
								reqVersion = true;
							}
						}
					}
				}
				radarActivityFlag = false;
			}
		}
	}

	// Выдача команды выбора режима радара [STREAM | REQUEST]
	// STREAM - Выдача отметок радара потоком, без запросов
	// REQUEST - Выдача отметок только по запросу. Выдаются отметки 
	// соответствующие текущему кадру радара.

	bool setMode(TCogRadarSendMode sendMode, TCogRadarTrackingMode trackingMode) {
		if (dataProvider) {
			std::lock_guard<std::mutex> lock(dataProviderMutex);
			return dataProvider->write(cprrProtocol.reqSetMode(
				true, // Включение ЛЧМ
				sendMode == TCogRadarSendMode::STREAM, // Режим выдачи отметок
				trackingMode == TCogRadarTrackingMode::ON).pack(std::stringstream{}));
		} else {
			return false;
		}
	}

public:

	TCogRadarCPRR() {	};

	~TCogRadarCPRR() {
		if (dataProvider) {
			close();
		}
	};

	TCogRadarInfo& getVersion() {
		return radarInfo;
	}

	TCogRadarVersionFrmt& getSoftVersion() {
        return softInfo;
    }        

	TCogRadarVersionFrmt& getHardVersion() {
		return hardInfo;
	}

	bool setPlatformInfo(const TCogRadarSetPlatform& info) {
		return true;
	}

	bool start(TCogRadarSendMode sendMode, TCogRadarTrackingMode trackingMode, TCogFrameEvent frameDoneEvent, int userArg) {
		if (getRadarStatus() == TCogRadarStatus::ONLINE) {
			userFrameDoneEvent = frameDoneEvent;
			userFrameDoneArg = userArg;
			return setMode(sendMode, trackingMode);
		} else {
			return false;
		}
	}

	//
	bool stop() {
		if (dataProvider) {
			std::lock_guard<std::mutex> lock(dataProviderMutex);
			return dataProvider->write(cprrProtocol.reqSetMode(false, false, true).pack(std::stringstream{})) > 0;
		}
		return false;
	}

	// Резерв. Универсальный метод установки параметров радара
	bool set(const std::string& cmdString) {
		return true;
	}

	// Резерв. Универсальный метод запроса параметров радара
	std::string get(const std::string& cmdString) {
		return "";
	}

	// Принимаем опции работы
	bool setOption(const std::string& key, const std::string& value) {
		if (dataProvider) {
			// Если канал связи уже открыт, установка опций не возможна.
			return false;
		}
		options[key] = value;
		return true;
	}
	// Принимаем опции работы
	bool setOption(const std::string& key, int value) {
		return setOption(key, std::to_string(value));
	}


	// Функция "открытия" радара. Установка связи и тест наличия радара.
	// Параметры: 
	// ip - адрес радара (например: "192.168.1.4")
	// port - порт.

	TCogRadarStatus open(const std::string& ip, uint16_t port) {

		// Повторное открытие недопустимо. dataProvider в качестве флага двойного открытия
		if (dataProvider) {
			return TCogRadarStatus::INCORRECT;
		}

		// Инициализация переменных состояния
		lastErrorCode = 0;
		doExit = false;
		radarInfoUpdate = false;
		networkError = false;
		setRadarStatus(TCogRadarStatus::OFFLINE);

		TCogDataProviderStatus dataProviderOpenStatus = TCogDataProviderStatus::CONNECT_ERROR;

		// критическая секция для работы с dataProvider
		{
			std::lock_guard<std::mutex> lock(dataProviderMutex);

			// В зависимости от опции "TRANSPORT" выбираем драйвер канала связи [TCP | UDP]
			if (options["TRANSPORT"] == "TCP") {
				// Связь с радаром по TCP
				dataProvider = std::make_unique<TCogTCPDataProvider>();
			}
			else {
				// Связь с радаром по UDP
				dataProvider = std::make_unique<TCogUDPDataProvider>();
			}

			// Передача параметров IP:PORT в dataProvider
			dataProvider->init(ip, port, options);

			// открываем драйвер связи. Передаем обработчик приема данных и 
			// обработчик приема статуса для управления статусом радара
			dataProviderOpenStatus = dataProvider->open(
				// Лямбда-функция для перенаправления события на уровень выше
				[this](void* buffer, unsigned int size) {
					this->rxEvent(buffer, size);
				},
				// Лямбда-функция для контроля ошибок в канале связи, что бы скинуть статус радара в OFFLINE
					[this](const TCogDataProviderStatus& status) {
					if ((status == TCogDataProviderStatus::DISCONNECTED) ||
						(status == TCogDataProviderStatus::READ_ERROR) ||
						(status == TCogDataProviderStatus::WRITE_ERROR)) {
						setRadarStatus(TCogRadarStatus::OFFLINE);
					}
				});
		}

		if (dataProviderOpenStatus == TCogDataProviderStatus::OPEN_ERROR) {
			// Если ошибка при открытии сокета, то бросаем исключение! дальше работать нельзя.
			throw std::runtime_error("network error!");
		} else {
			if (dataProviderOpenStatus == TCogDataProviderStatus::CONNECTED) {
				// Если соединение с радаром установлено (TCP) или просто открыт сокет (UDP) 
				// Делаем запросы к радару что бы определить статус радара [ONLINE | OFFLINE]
				for (int i = 0; i < CPRR_RADAR_GETVER_TRY_NUM; i++) {
					if (radarInfoUpdate || networkError) {
						// есть ошибка отправки запроса или радар ответил
						// дальнейшие попытки не нужны
						break;
					}
					
					// Формируем пакет запроса версии и передаем его для отправки
					if (dataProvider->write(cprrProtocol.reqGetVersion().pack(std::stringstream{})) < 0) {
						networkError = true;
					}
					
					// пауза между попытками
					std::this_thread::sleep_for(std::chrono::milliseconds(CPRR_RADAR_GETVER_DELAY_MS));
				}
			}
			// Статус запуска цикла мониторинга статуса
			isReadingStatusRun = false;
			//Запуск потока мониторинга статуса радара, для формирования статуса ONLINE
			readingStatusThread = std::make_unique<std::thread>([this]() { this->readingStatusLoop(); });
		}
		return getRadarStatus();
	}


	bool close() {
		if (dataProvider) {
			std::lock_guard<std::mutex> lock(dataProviderMutex);
			doExit = true;
			radarInfoUpdate = false;
			if (readingStatusThread) {
				// Ждем пока завершиться readingLoop()...
				readingStatusIntervalSignal.notify_one();
				// isReadingStatusRun гарантирует, что поток опроса статуса запустился
				while (!isReadingStatusRun)
					std::this_thread::sleep_for(std::chrono::milliseconds(1));

				readingStatusThread->join();
				readingStatusThread.reset(nullptr);
			}
			dataProvider->close();
			dataProvider.reset(nullptr);
			return true;
		} else {
			return false;
		}
	}

	void rxEvent(void* buffer, unsigned int size) {
		std::stringstream rxData;

		if (size == (unsigned int)-1) {
			networkError = true;
		} else {
			rxData.write((const char*)buffer, size);
			rxEvent(rxData);
		}
	}

	void rxEvent(std::istream& in) {
		cprrProtocol.unpack(in, [this](void* buffer, unsigned int size) {
			this->rxCmd((TRadarPacket*)buffer, size);
			});
	}

	TCogRadarStatus getRadarStatus() {
		return radarStatus;
	}

	bool subscribeRadarStatus(TCogRadarStatusEvent radarStatusEvent, int userArg) {
		userRadarStatusEvent = radarStatusEvent;
		userRadarStatusArg = userArg;
		return true;
	}
	bool unSubscribeRadarStatus() {
		userRadarStatusEvent = NULL;
		userRadarStatusArg = 0;
		return true;
	}

private:

	void rxCmd(TRadarPacket* pkt, unsigned int size) {
		static uint64_t frameid = -1;
		radarActivityFlag = true;

		if (pkt->hdr.pktType == RADAR_PKT_INFO_ID) {
			// GET VERSION 
			std::stringstream stream;
            softInfo.major = pkt->message.verInfo.SoftVersion.major;
            softInfo.minor = pkt->message.verInfo.SoftVersion.minor;
            softInfo.patch = pkt->message.verInfo.SoftVersion.patch;
			hardInfo.major = pkt->message.verInfo.HardVersion.major;
			hardInfo.minor = pkt->message.verInfo.HardVersion.minor;
			radarInfo.HardVersion = std::to_string(hardInfo.major) + "." + std::to_string(hardInfo.minor);
			radarInfo.SoftVersion = std::to_string(softInfo.major) + "." + std::to_string(softInfo.minor)+ "." + std::to_string(softInfo.patch);
			stream << (unsigned long)pkt->message.verInfo.SerialNumb;
			radarInfo.SerialNumb = stream.str();
			radarInfoUpdate = true;
		}
		// После обработки пакета версии, делаем установку статуса радара, 
		// что бы дать возможность пользователю запросить версию радара в событии статуса,
		// если пользователь подписался на рассылку статуса
		if (radarInfoUpdate)
			setRadarStatus(TCogRadarStatus::ONLINE);

		if (pkt->hdr.pktType == RADAR_PKT_ERROR_ID) {
			// От радара получен код ошибки
			lastErrorCode = pkt->message.error.Error;
			if (lastErrorCode)
				if (userRadarStatusEvent != NULL) {
					userRadarStatusEvent(TCogRadarStatus::INCORRECT, userRadarStatusArg);
				}
		}

		if (pkt->hdr.pktType == RADAR_PKT_DATA_ID) {
			//
			int targetNum = (size - sizeof(TRadarTargetParam) - sizeof(TRadarHeaderMessage)) / sizeof(TRadarTarget);
			// отметки одного кадра идут с одинаковым GrabNumber. 
			// Пакет с 0 кол-вом отметок признается за конец кадра
			// Учитывая эти два признака разбиваем поток отметок по кадрам
			if ((frameid != pkt->message.data.GrabNumber) || (targetNum == 0)) {
				if (userFrameDoneEvent != NULL) {
					static uint64_t frameidDone = -1;
					if (frameidDone != frameid) {
						userFrameDoneEvent(pkt->message.data.TimeStamp, frameid, radarTargets, userFrameDoneArg);
						frameidDone = frameid;
					}
				}
				// Пришли отметки нового кадра
				radarTargets.clear();
				frameid = pkt->message.data.GrabNumber;
			}
			for (int i = 0; i < targetNum; i++) {
				TRadarTarget* target = &pkt->message.data.Targets[i];
				radarTargets.push_back({
					target->ObjID,
					target->Range,
					target->Azimuth,
					target->LiveTime,
					target->Rcs,
					target->Xcoord,
					target->Xrate,
					target->Xacceleration,
					target->Ycoord,
					target->Yrate,
					target->Yacceleration
					});
			}
		}
	}
};


#endif
