#ifndef _COG_UDPDATAPROVIDER_
#define _COG_UDPDATAPROVIDER_

#include <string>
#include <sstream>
#include <iostream>
#include <thread>
#include <functional>
#include <chrono>

#include <cogDataProvider.h>
#include "driver/DatagramSocket.h"

class TCogUDPDataProvider : public TCogDataProvider {
public:

    bool init(const std::string& ip, uint16_t port, const std::map<std::string, std::string>& options) {
        IP = ip;
        this->port = port;
        if (options.find("PORT_LOCAL") != options.end()) {
            // Возможность задать собственный порт (требуется в некоторых тестовых задачах)
            int value = std::atoi(options.at("PORT_LOCAL").c_str());
            if (value > 0) {
                port_local = value;
            }
        }
        return true;
    }

    int write(std::istream& stream) {
        if (doExit) return 0;
        int result = 0;
        stream.seekg(0, stream.end);
        int length = (int)stream.tellg();
        stream.seekg(0, stream.beg);
        // -------------------------
        if (length > 0) {
            auto buffer = std::make_unique<char[]>(length);
            stream.read(buffer.get(), length);
            result = client->sendTo(buffer.get(), length, IP.c_str());
        }
        return result;
    }

    int write(void * buffer, unsigned int size) {
        std::stringstream data;
        data.write((const char *) buffer, size);
        return write(data);
    }

    TCogDataProviderStatus open(TCogGetBufferEvent rxEvent, TCogDataProviderStatusEvent statusEvent) {
        // Двойное открытие недопустимо
        if (!client) {
            userRxEvent = rxEvent;
            userStatusEvent = statusEvent;

            client = std::make_unique<DatagramSocket>(port, port_local, IP.c_str(), false, false);

            if (client) {
                if (client->getLastError() < 0) {
                    client.reset(nullptr);
                    return TCogDataProviderStatus::OPEN_ERROR;
                }
                readingThread = std::make_unique <std::thread>([this] {
                    this->readingLoop(); });
                return TCogDataProviderStatus::CONNECTED;
            }
        }
        return TCogDataProviderStatus::OPEN_ERROR;
    }

    bool close() {
        if (client) {
            doExit = 1;
            client->SocketClose();
            client.reset(nullptr);
            if (readingThread) {
                readingThread->join();
                readingThread.reset(nullptr);
            }
            return true;
        }
        return false;
    }

private:
    volatile bool doExit = false;
    std::unique_ptr<DatagramSocket> client;
    std::unique_ptr<std::thread> readingThread;
    TCogGetBufferEvent userRxEvent;
    TCogDataProviderStatusEvent userStatusEvent = NULL;
    std::string IP = "127.0.0.1";
    int port = 7000;
    int port_local = 0;

    void readingLoop() {
        doExit = 0;
        char msg[2000];
        while (doExit == 0) {
            if (!client) {
                return;
            }
            int size = client->receive(msg, sizeof (msg));
            if ((doExit == false)||(size == -1)) {
                if (userRxEvent != NULL) {
                    // size : -1 for error
                    userRxEvent((void*)&msg, size);
                }
            }
        }
        return;
    }
};

#endif
