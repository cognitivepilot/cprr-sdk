#ifndef _COG_TCPDATAPROVIDER_
#define _COG_TCPDATAPROVIDER_

#include <string>
#include <sstream>
#include <iostream>
#include <thread>
#include <functional>
#include <chrono>

#include <cogDataProvider.h>
#include "driver/tcpClient.h"


// Пауза между попытками соединиться с сервером
#define CLIENT_RECONNECT_INTERVAL 2000


class TCogTCPDataProvider : public TCogDataProvider {
private:
    bool doExit = false;
    std::unique_ptr<TCPClient> client;
    TCogGetBufferEvent userRxEvent;
    TCogDataProviderStatusEvent userStatusEvent = NULL;
    std::string IP = "127.0.0.1";
    int port = 7000;

public:
    // Прием параметров соединения.
    // ip   : Строка ip адреса ("192.168.1.4")
    // port : порт радара
    // options : резерв. (не используется. список опций можно получить TCogParser::getParams(options)
    bool init(const std::string& ip, uint16_t port, const std::map<std::string, std::string>& options) {
        IP = ip;
        this->port = port;
        return true;
    }

    // Запись в канал связи исполюзуя поток
    int write(std::istream& stream) {
        if (doExit) {
            return 0;
        }

        int result = 0;
        stream.seekg(0, stream.end);
        int length = (int)stream.tellg();
        stream.seekg(0, stream.beg);
        
        if (length > 0) {
            auto buffer = std::make_unique<char[]>(length);
            stream.read(buffer.get(), length);
            if (client) {
                result = client->write(buffer.get(), length);
            }
        }
        return result;
    }

    // передача буфера в канал связи
    int write(void * buffer, unsigned int size) {
        std::stringstream data;
        data.write((const char *) buffer, size);
        return write(data);
    }

    // Вход:
    // rxEvent     - колбэк для получения данных;
    // statusEvent - колбэк для получения событий об изменении статуса соединения (канала данных);
    // Возврат: 
    // CONNECTED     - соединение с радаром установлено;
    // CONNECT_ERROR - ошибка установки соединения, но сокет открыт;
    // OPEN_ERROR    - ошибка сокета.
    TCogDataProviderStatus open(TCogGetBufferEvent rxEvent, TCogDataProviderStatusEvent statusEvent) {
        userRxEvent = rxEvent;
        userStatusEvent = statusEvent;
        client = std::make_unique<TCPClient>(IP, port, CLIENT_RECONNECT_INTERVAL,
            [this](const TCPClientState& state) {
                if (userStatusEvent) {
                    if (state == TCPClientState::CONNECTED)
                        userStatusEvent(TCogDataProviderStatus::CONNECTED);
                    else if (state == TCPClientState::CONNECT_ERROR)
                        userStatusEvent(TCogDataProviderStatus::CONNECT_ERROR);
                    else if (state == TCPClientState::DISCONNECTED)
                        userStatusEvent(TCogDataProviderStatus::DISCONNECTED);
                    else if (state == TCPClientState::READ_ERROR)
                        userStatusEvent(TCogDataProviderStatus::READ_ERROR);
                    else if (state == TCPClientState::WRITE_ERROR)
                        userStatusEvent(TCogDataProviderStatus::WRITE_ERROR);
                }
            }, [this](char* data, int size) {
                if (userRxEvent && size)
                    userRxEvent((void*)data, (unsigned int)size);
            });
            

        int result = client->open();

        return (result < 0) ? TCogDataProviderStatus::OPEN_ERROR : (result) ? TCogDataProviderStatus::CONNECTED : TCogDataProviderStatus::CONNECT_ERROR;
    }

    bool close() {
        doExit = true;
        if (client) {
            client->close();
        }
        return true;
    }
};

#endif
