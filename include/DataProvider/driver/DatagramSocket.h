#ifndef DATAGRAMSOCKET_H_INCLUDED
#define DATAGRAMSOCKET_H_INCLUDED

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE


#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#endif

#define STRCPY_MAX_NUM  255


#ifdef WIN32
typedef int socklen_t;
#endif

class DatagramSocket {
private:
#ifdef WIN32
    SOCKET sock;
#else
    int sock;
#endif
    long lastError = 0;
    sockaddr_in outaddr;
    char received[STRCPY_MAX_NUM];
    int sockClosed;

    void strcpy_s(char* dst, size_t size, const char* src) {
        if (!dst || !src) return;
        for (; size > 0; --size) {
            if (!(*dst++ = *src++)) return;
        }
    }

public:

    DatagramSocket(int port_dest, int port_src, const char* address, bool broadcast, bool reusesock) {

#ifdef WIN32
        WSAData wsaData;
        if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
            throw std::runtime_error("Could not find a usable WinSock DLL");
        }
#endif

        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        sockClosed = 0;

        //set up address to use for sending
        memset(&outaddr, 0, sizeof (outaddr));
        outaddr.sin_family = AF_INET;
        outaddr.sin_addr.s_addr = inet_addr(address);
        outaddr.sin_port = htons(port_dest);

        int OptVal = 1;

        if (broadcast) {
            setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*)&OptVal, sizeof(OptVal));
        }

        if (reusesock) {
            setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&OptVal, sizeof(OptVal));

        }

        if (port_src) {
            sockaddr_in addr;
            // Если требуется явно указать собственный порт:
            //set up bind address
            memset(&addr, 0, sizeof(addr));
            addr.sin_family = AF_INET;
            addr.sin_addr.s_addr = htonl(INADDR_ANY);
            addr.sin_port = htons(port_src);

            if (::bind(sock, (struct sockaddr*)&addr, sizeof(addr))) {
                lastError = -1;
            }
        }

    }

    ~DatagramSocket() {
        if (sockClosed == 0) {
            SocketClose();
        }
    }

    void SocketClose() {
#if WIN32
        closesocket(sock);
        WSACleanup();
#else
        shutdown(sock, SHUT_RD);
        close(sock);
#endif
        sockClosed = 1;
    }

    long receive(char* msg, int msgsize) {
        if (sockClosed) return 0;
        struct sockaddr_in sender;
        socklen_t sendersize = sizeof (sender);
        int retval = recvfrom(sock, msg, msgsize, 0, (struct sockaddr *) &sender, &sendersize);
        strcpy_s(received, STRCPY_MAX_NUM, inet_ntoa(sender.sin_addr));
        return retval;
    }
#undef STRCPY_MAX_NUM

    char* received_from() {
        return received;
    }

    long send(const char* msg, int msgsize) {
        return sendto(sock, msg, msgsize, 0, (struct sockaddr *) &outaddr, sizeof (outaddr));
    }

    long sendTo(const char* msg, int msgsize, const char* addr) {
        outaddr.sin_addr.s_addr = inet_addr(addr);
        return sendto(sock, msg, msgsize, 0, (struct sockaddr *) &outaddr, sizeof (outaddr));
    }
    long getLastError(void) {
        return lastError;
    }

};


#endif // DATAGRAMSOCKET_H_INCLUDED
