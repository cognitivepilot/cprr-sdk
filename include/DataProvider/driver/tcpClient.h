#pragma once
#include <functional>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#endif

#define STRCPY_MAX_NUM  255

#define TCP_READ_BUFFER_SIZE  1024

// Возможные события клиента.
enum class TCPClientState { CONNECTED, CONNECT_ERROR, DISCONNECTED, READ_ERROR, WRITE_ERROR};

typedef std::function<void(const TCPClientState& state) > TCPClientNotify;
typedef std::function<void(char * data, int size) > TCPClientRxNotify;


#ifdef WIN32
typedef int socklen_t;
typedef SOCKET TSocket;
#else
typedef int TSocket;
#endif


// Класс фонового чтения из сокета
class TCPSocketReceiver {
private:
    bool doExit = false;
    TSocket sock;
    std::unique_ptr<std::thread> readingThread;
    TCPClientRxNotify rxEvent;
    std::mutex readingLoopMutex;

    int readBuffer(char * buffer, unsigned int size) {
        unsigned char buf;
        // Ожидающая функция чтения TCP
        int result = recv(sock, (char*)&buf, 1, MSG_PEEK);
        if (result > 0) {
            // чтение буфера, как только что-то пришло
            return recv(sock, buffer, size, 0);
        } else {
            // Ошибка
#ifdef WIN32
            int errorCode = WSAGetLastError();
            if (errorCode != WSAECONNABORTED) {
                // std::cout << " [sock error: " << errorCode << "] " << std::flush;
            }
#endif
            return -1;
        }
    }

    void readingLoop() {
        // захватываем мьютекс до выхода из readingLoop. Пока мьютекс не освободится дуструктор будет ждать
        std::lock_guard<std::mutex> lock(readingLoopMutex);

        char rxBuffer[TCP_READ_BUFFER_SIZE];

        while (!doExit) {
            int readResult = readBuffer(rxBuffer, TCP_READ_BUFFER_SIZE);
            if (readResult == -1) {
                // Сигнализация об ошибке и выход из цикла опроса
                rxEvent(NULL, 0);
                break;
            } else {
                rxEvent(rxBuffer, readResult);
            }
        }
    }

public:

    TCPSocketReceiver(TSocket tcpSock, TCPClientRxNotify rxDataEvent) : sock (tcpSock), rxEvent(rxDataEvent) {
        readingThread = std::make_unique<std::thread>([this]() { this->readingLoop(); });
    }

    ~TCPSocketReceiver() {
        doExit = true;
        // Ждем пока завершиться readingLoop()...
        std::lock_guard<std::mutex> lock(readingLoopMutex);
        readingThread->join();
    }
};

class TCPClientSocket {
private:
    TSocket sock = 0;
    volatile bool sockOpened = false;
    bool connected = false;
    // Событие изменения состояния
    TCPClientNotify notifyEvent;
    // Событие получения данных
    TCPClientRxNotify rxEvent;
    std::unique_ptr<TCPSocketReceiver> socketReceiver;

public:

    TCPClientSocket(TCPClientNotify event, TCPClientRxNotify rxEvent) : notifyEvent(event), rxEvent(rxEvent) { }

    ~TCPClientSocket() {
        if (sockOpened) {
            close();
        }
    }

    int open(const char* address, int port) {
        // Повторный вызов open не допустим
        if (sockOpened) {
            return -1;
        }

        connected = false;

        sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (sock == -1) {
            return -1;
        }
        // сокет открыт
        sockOpened = true;

        sockaddr_in outaddr;
        memset(&outaddr, 0, sizeof(outaddr));
        outaddr.sin_family = AF_INET;
        outaddr.sin_addr.s_addr = inet_addr(address);
        outaddr.sin_port = htons(port);

        int OptVal = 1;
        // для режима взаимодействия с радаром установим TCP_NODELAY
        setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*)&OptVal, sizeof(OptVal));
    
        struct timeval timeout;
        timeout.tv_sec = 1;  // Сделаем ограничение на время установки соединения. 1с
        timeout.tv_usec = 0;
        setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));

        // Попытка установить соединение с сервером
        if (connect(sock, (sockaddr*)&outaddr, sizeof(outaddr)) != 0) {
            // Ошибка. Не получилось установить соединение
            notifyEvent(TCPClientState::CONNECT_ERROR);
            return -1;
        } else {
            // соединение установлено
            connected = true;

            // Создадим объект прослушивания сокета. 
            socketReceiver = std::make_unique<TCPSocketReceiver>(sock, [this](char* data, int size) {
                if (size) {
                    // Данные пришли. Передаем их потребителю
                    rxEvent(data, size);
                } else {
                    // ошибка во время чтения. TCPClient получив событие ошибки должен попросить сделать пересоединение 
                    connected = false;
                    notifyEvent(TCPClientState::READ_ERROR);
                }
                });

            notifyEvent(TCPClientState::CONNECTED);
        }
        return 0;
    }


    void close() {
        bool disconnectionNotify = connected;

        if (!sockOpened) {
            return;
        }

        connected = false;
#if WIN32
        closesocket(sock);
#else
        shutdown(sock, SHUT_RD);
        ::close(sock);
#endif
        // Уничтожим объект опроса сокета
        if (socketReceiver) {
            socketReceiver.reset(nullptr);
        }

        sockOpened = false;

        if ((notifyEvent != NULL) && disconnectionNotify) {
            notifyEvent(TCPClientState::DISCONNECTED);
        }
    }

    int write(const char* msg, int msgsize) {
        if (connected) {
            return send(sock, msg, msgsize, 0);
        } else {
            return -1;
        }
    }
};

class TCPClient {
private:
    volatile bool doExit = false;
    bool isOpen = false;
    bool isConnected = false;
    // параметры соединения
    std::string ip;
    int port;
    int reConnectInterval;
    TCPClientNotify event;
    // Событие получения данных
    TCPClientRxNotify rxEvent;

    std::unique_ptr<std::thread> reconnectionThread;
    std::unique_ptr<TCPClientSocket> clientSocket;

    std::mutex reConnectMutex;
    std::condition_variable reConnectSignal;
    std::mutex retryConnectMutex;
    std::condition_variable retryConnectSignal;
    volatile bool isFirstConnectResult = false;
    
    void connectionLoop() {
        std::unique_lock<std::mutex> reConnectLock(reConnectMutex);
        std::unique_lock<std::mutex> retryConnectLock(retryConnectMutex);
        
        while (!doExit) {
            // Попытка открыть сокет и установить соединение с сервером
            if (clientSocket->open(ip.c_str(), port) == 0) {
                // Соединение установлено. 
                isConnected = true;
                reConnectSignal.wait(reConnectLock);
            }
            // Закрываем существующий сокет
            clientSocket->close();
            
            // пауза перед следующей попыткой установить соединение (возможность прервать паузу)
            if (!doExit) {
                retryConnectSignal.wait_for(retryConnectLock, std::chrono::milliseconds(reConnectInterval));
            }
        }
    }
    void doReconnect() {
        reConnectSignal.notify_one();
    }

public:

    TCPClient(const std::string& address, int port, int reConnectInterval, TCPClientNotify event, TCPClientRxNotify rxEvent) : ip(address),
        port(port), reConnectInterval(reConnectInterval), event(event), rxEvent(rxEvent){ 
#ifdef WIN32
        WSAData wsaData;
        if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
            throw std::runtime_error("Could not find a usable WinSock DLL");
        }
#endif
    }

    ~TCPClient() {  
        if (isOpen) {
            close();
        }
#ifdef WIN32
        WSACleanup();
#endif
    }
    
    // Открываем драйвер TCP клиента. 
    // Возврат: 0 - открыт, но нет соединения
    //          1 - открыт, соединение установлено
    //         -1 - Ошибка
    int open() {

        if (isOpen) {
            return -1;
        }

        isOpen = true;

        clientSocket = std::make_unique<TCPClientSocket>([this](const TCPClientState& state) {
            if (event) {
                event(state);
            }
            // Обработка первой попытки (вызов open())
            if ((state == TCPClientState::CONNECT_ERROR) ||
                (state == TCPClientState::CONNECTED)) {
                isFirstConnectResult = true;
            }

            // Обработка ошибок на сокете
            if ((state == TCPClientState::CONNECT_ERROR) ||
                (state == TCPClientState::DISCONNECTED) ||
                (state == TCPClientState::WRITE_ERROR) ||
                (state == TCPClientState::READ_ERROR)) {
                isConnected = false;
                if (!doExit) doReconnect();
            }
            }, [this](char* data, int size) { rxEvent(data, size); });

        isFirstConnectResult = false;
        reconnectionThread = std::make_unique<std::thread>([this]() { this->connectionLoop(); });
        // Нужно дождаться результата первой попытки соединения (вызов open() цикле connectionLoop )
        int waitInterval = 0;
        while (!isFirstConnectResult && (waitInterval < reConnectInterval)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            waitInterval += 10;
        }

        return (isConnected) ? 1 : 0;
    }

    int write(void* buffer, unsigned int size) {
        if (isConnected && isOpen)
            return clientSocket->write((const char *)buffer, size);
        return -1;
    }

    int close() {
        if (isOpen) {
            doExit = 1;
            // выводим поток управления соединением из ожидания, если он находится в ожидании timeout
            retryConnectSignal.notify_one();
            // Посылаем сигнал пробуждения потока. Ожидаем его завершения
            doReconnect();
            reconnectionThread->join();
            // закрываем сокет
            clientSocket->close();
            isOpen = false;
            return 0;
        } else {
            // попытка вызвать close() без open()
            return -1;
        }
    }
};
