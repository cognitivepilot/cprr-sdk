
#ifndef _COG_DATAPROVIDER_
#define _COG_DATAPROVIDER_

#include <string>
#include <functional>
#include <sstream>
#include <iostream>
#include <map>

/*
----------------------------------------------------------------
    Статус     |     UDP     |     TCP     |
----------------------------------------------------------------
  CONNECTED    |      -      |      +      |
CONNECT_ERROR  |      -      |      +      |
 DISCONNECTED  |      -      |      +      |
  READ_ERROR   |      +      |      +      |
 WRITE_ERROR   |      +      |      +      |
  OPEN_ERROR   |      +      |      +      |
----------------------------------------------------------------
*/
enum class TCogDataProviderStatus { CONNECTED, CONNECT_ERROR, DISCONNECTED, READ_ERROR, WRITE_ERROR, OPEN_ERROR };

std::ostream& operator<<(std::ostream& out, const TCogDataProviderStatus value) {
    if (value == TCogDataProviderStatus::CONNECTED)
        return out << "CONNECTED";
    else if (value == TCogDataProviderStatus::CONNECT_ERROR)
        return out << "CONNECT_ERROR";
    else if (value == TCogDataProviderStatus::DISCONNECTED)
        return out << "DISCONNECTED";
    else if (value == TCogDataProviderStatus::READ_ERROR)
        return out << "READ_ERROR";
    else if (value == TCogDataProviderStatus::WRITE_ERROR)
        return out << "WRITE_ERROR";
    else if (value == TCogDataProviderStatus::OPEN_ERROR)
        return out << "OPEN_ERROR";
    return out << "UNDEF";
}


typedef std::function<void (const TCogDataProviderStatus& status) > TCogDataProviderStatusEvent;
typedef std::function<void (void * buffer, unsigned int size)> TCogGetBufferEvent;

class TCogDataProvider {
public:
    virtual ~TCogDataProvider() {};
    virtual bool init(const std::string& ip, uint16_t port, const std::map<std::string, std::string>& options) = 0;
    virtual int  write(void * buffer, unsigned int size) = 0;
    virtual int  write(std::istream& stream) = 0;
    virtual TCogDataProviderStatus open(TCogGetBufferEvent rxEvent, TCogDataProviderStatusEvent statusEvent) = 0;
    virtual bool close() = 0;
};

#endif

