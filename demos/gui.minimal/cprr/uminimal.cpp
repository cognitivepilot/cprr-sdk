/*
 Пример использования SDK радара для gui-приложения.

 Данный пример по сути повторяет пример "\demos\cprr", где
 вместо вывода списка отметок на консоль, массив отметок передается
 в скрипт отрисовки (res\monitoring.htm) через механизм sciter SDK. 
 Для более подробной справки функционалу sciter SDK обратитесь к 
 https://sciter.com/

 Для более подробной информации по использованию radar SDK обратитесь 
 к примеру "\demos\cprr"
*/

#include "sciter-x.h"
#include "sciter-x-graphics.hpp"
#include "sciter-x-window.hpp"
#include "sciter-om.h"
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <mutex>
// 
#include <radarsdk.h>

#include "sciter-win-main.cpp"

#define CONFIG_FILE_NAME "uminimal.cfg"

using namespace std;

volatile bool doExit = false;

std::unique_ptr<TCogRadarX> cprr;

static struct {
    std::string IP;
    uint16_t port;
    float radius;
    bool demo;
    bool isTracking; // режим трекинга в конфиг. файле
} config = {"192.168.1.4", 7000, 100.0, false, true };


static struct {
    std::mutex  mtx; 
    std::string status;
    int   fps;
    int   fpsTmpCount;
    std::vector<TCogRadarTarget> targets;
} currentData;

// Параметры окна gui
static RECT wrc = {100, 100, 1000, 1000};

class frame : public sciter::window {
public:
    frame() : window(SW_TITLEBAR | SW_RESIZEABLE | SW_CONTROLS | SW_MAIN, wrc) {
    } //| SW_TOOL | SW_ENABLE_DEBUG

    // Регистритуем метод, доступный для сызова из gui-скрипта (см. monitoring.htm)
    SOM_PASSPORT_BEGIN(frame)
    SOM_FUNCS(SOM_FUNC(getCurrentPoints))
    SOM_PASSPORT_END

    // взаимодействие с gui скриптом. вызов через view.frame.getCurrentPoints(...)
    sciter::value getCurrentPoints(sciter::value reqId) {
        if (reqId == 0) {
            // Запрос отметок от gui: нужно выдать массив отметок
            std::vector<sciter::value> tPoints;
            sciter::value arrayOfTargets;
            
            std::lock_guard<std::mutex> lock(currentData.mtx);
            if (currentData.targets.size()) {
                for (const TCogRadarTarget& tg : currentData.targets) {
                    tPoints.push_back(sciter::value((float)tg.Xcoord));
                    tPoints.push_back(sciter::value((float)tg.Ycoord));
                }
                arrayOfTargets = sciter::value::make_array(UINT(tPoints.size()), &tPoints[0]);
            } else 
                arrayOfTargets = sciter::value::make_array(UINT(0), NULL);
            return sciter::value(arrayOfTargets);
        }
        else 
            if (reqId == 1) {
                // Запрос статуса от gui: нужно выдать строку статуса
                std::lock_guard<std::mutex> lock(currentData.mtx);
                sciter::string status = u2w(currentData.status);
                return sciter::value(status);
            }
            else {
                // Запрос fps: нужно выдать значение текущего fps
                return sciter::value((int)currentData.fps);
            }
    }
};

#include "resources.cpp"

frame* guiFrame;

auto frameDone = [&](uint64_t timeStamp, uint64_t grabNumber, vector<TCogRadarTarget>& targets, int userArg) {
    if (!doExit) {
        stringstream frameInfo;
        frameInfo << "Time: " << std::fixed << std::setprecision(1) << (timeStamp / 1000000.0) << " s, Frame #" << grabNumber;
        std::lock_guard<std::mutex> lock(currentData.mtx);
        currentData.fpsTmpCount++;
        currentData.targets = targets;
        currentData.status = frameInfo.str();
    }
};

// ламбда-функция обработки изменения статуса радара
auto radarStatusNotify = [](TCogRadarStatus status, int userArg) {
    if (cprr && guiFrame) {

        sciter::dom::element root = guiFrame->get_root();

        if (status == TCogRadarStatus::ONLINE) {
            string radarVersion = " Version Hardware: " + cprr->getVersion().HardVersion + " Version Software: " + \
                cprr->getVersion().SoftVersion + " SN : " + cprr->getVersion().SerialNumb;
            
            // Передадим в gui состояние радара и его версию
            root.call_function("SetRadarStatus", sciter::value((int)1));
            root.call_function("SetStatusText", sciter::value(radarVersion));

            cprr->start(TCogRadarSendMode::STREAM,
                (config.isTracking) ? TCogRadarTrackingMode::ON : TCogRadarTrackingMode::OFF, frameDone, 0);
        } else {
            // Радар OFFLINE. 
            root.call_function("SetRadarStatus", sciter::value((int)0));
        }
    }
};

int uimain(std::function<int() > run) {

    sciter::archive::instance().open(aux::elements_of(resources)); // bind resources[] (defined in "resources.cpp") with the archive

    guiFrame = new frame(); // Объект доступа к окну. Удаляется сам.

    guiFrame->load(WSTR("this://app/monitoring.htm"));
    guiFrame->expand();

    sciter::dom::element root = guiFrame->get_root();

    // загрузка конфигурации из файла
    auto configParam = TCogParser::getParams(TCogParser::configFile2Str(CONFIG_FILE_NAME));
    config.IP = configParam["IP"];
    config.port = std::atoi(configParam["PORT"].c_str());
    config.demo = configParam["DEMO"] == "ON";
    config.isTracking = configParam["TRACKING"] == "ON";
    std::istringstream radiusConfigStr(configParam["RADIUS"]);
    radiusConfigStr >> config.radius;

    // Вывод некоторых настроек в заголовок
    root.call_function("SetStatusText", sciter::value(string("Connecting to IP:") + config.IP + " : " + to_string(config.port) + " TRANSPORT: [" + configParam["TRANSPORT"]+"]"));

    // Вызов функции скрипта (см. monitoring.htm) для настройки желаемого радиуса отображения отметок. 
    root.call_function("SetRadarRadius", sciter::value(config.radius));

    cprr = std::make_unique<TCogRadarX>((config.demo) ? COG_RADAR_DEMO : COG_RADAR_CPRR);

    // Передача опций выбора транспорта в драйвер радара
    cprr->setOption("TRANSPORT", configParam["TRANSPORT"]);

    // Подписка на изменение статуса радара
    cprr->subscribeRadarStatus(radarStatusNotify, 0);

    // для контроля FPS создаем поток
    auto fpsMonitor = std::make_unique<std::thread>([&] {
        while (!doExit) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            currentData.fps = currentData.fpsTmpCount;
            currentData.fpsTmpCount = 0;
        }
        });

    // Открываем канал связи с радаром и начинаем работу
    if (cprr->open(config.IP, config.port) == TCogRadarStatus::INCORRECT) {
        // Ошибка сокета. Индикация ошибки. Повторных попыток открыть сокет нет.
        root.call_function("SetRadarStatus", sciter::value((int)-1));
    }
    // работа с окном gui
    run();

    // пользователь закрыл окно. Закрываем драйвер радара
    doExit = true;
    cprr->unSubscribeRadarStatus();
    cprr->stop();
    cprr->close();

    fpsMonitor->join();
    return 0;
}

